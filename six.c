#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<curses.h>
#define TRUE 1
#define FALSE 0
#define COUNTOFSEATS 12

typedef struct flights
{
	int idOfSeat;
	int tecketReservation;
	char name[50];
	char surname[60];

}Flights;

void ShowTheFreeSeats(Flights *a)// Show number of free seets
{
	int countOfFreeSeats = 0;
	for (int i = 0; i < COUNTOFSEATS; i++)
	{
		if (a[i].tecketReservation == TRUE)
		{
			countOfFreeSeats++;
		}
	}
	printf("Count of free places:%d\n", countOfFreeSeats);
	getchar();
}



void ShowListOfFreeSeats(Flights *a)// show list of free places
{
	printf("\nList of free seats\n");
	printf("____________________\n");

	for (int i = 0; i < COUNTOFSEATS; i++)
	{
		
		if (a[i].tecketReservation == TRUE)
		{
			printf("The seat : \"%d\" is free\n",a[i].idOfSeat);
		}
	}
	printf("\nPress any key..\n");
	getchar();
}


void ShowListOfReservationSeats(Flights *a)
{
	printf("\nList of Reservation Seats\n");
	printf("_________________________\n");

	for (int i = 0; i < COUNTOFSEATS; i++)
	{
		if (a[i].tecketReservation == FALSE)
		{
			printf("The seat : \"%d\" is reserved by %s %s\n", a[i].idOfSeat, a[i].name, a[i].surname);
		}
	}
	printf("\nPress any key..\n");
	getchar();
}

void ReservSeat(Flights* a)
{
	int idOfS = 0;
	
	int numOfPlace = -1;

	printf("Enter seat id which you like to reserve:");
	scanf("%d", &idOfS);

	for (int i = 0; i < COUNTOFSEATS; i++)
	{
		if (a[i].idOfSeat == idOfS)
		{
			if (a[i].tecketReservation == TRUE)
			{
				
				a[i].tecketReservation = FALSE;
				numOfPlace = 0;
				printf("\nSeat can be booked.\n");

				printf("Enter your name:");
				
					scanf("%s", a[i].name);
					
				
				printf("\nEnter your Surname:");
				scanf("%s", a[i].surname);
				
				printf("\n Seat successfuly booked.\n");
				break;
			}
			else
			{	
				printf("The seat %d is booked. Try to choose another seat.", a[i].idOfSeat);
				numOfPlace = -2;
				break;
			}
		}
		
	
	}
	
	if (numOfPlace == -1)
	{
		printf("Check your input id.\n");
	}
	printf("\nPress any key..\n");
	char d = getchar();
	
}

void CancelReserve(Flights*a)
{
	char* name = (char*)malloc(sizeof(char));
	char* surname = (char*)malloc(sizeof(char));
	

	printf("Enter your name:");
	scanf("%s", name);
	
	printf("\nEnter your surname:");
	scanf("%s", surname);
	
	int check = FALSE;

	for (int i = 0; i < COUNTOFSEATS; i++)
	{
	
		if (a[i].tecketReservation == FALSE ) {
			if (strcmp(a[i].name, name) == FALSE && strcmp(a[i].surname, surname) == FALSE)
			{
				a[i].tecketReservation = TRUE;
				strcpy(a[i].name, "");
				strcpy(a[i].surname, "");
				printf("\nYou sucsseffuly cancel your reservation.\n");
				check = TRUE;
				break;
			}
		}
	}
	
	if (!check)
	{
		printf("The passenger %s %s is not pressented in list of passangers.\n", name, surname);
	}
	printf("\nPress any key..\n");
	getchar();
}

void menu()
{
	printf("\nMenu");
	printf("\n_________________________\n");
	printf("a)Show number of free places.\n");
	printf("b)Show list of free places.\n");
	printf("c)Show list of booked places.\n");
	printf("d)Reserve place.\n");
	printf("f)Cancel reserve.\n");
	printf("e)Exit fom program.\n");
}

int main(void)
{
	Flights *f = (Flights*)malloc(sizeof(Flights)*COUNTOFSEATS);

	
	for (int i = 0; i < COUNTOFSEATS; i++)
	{
		f[i].idOfSeat = i;
		f[i].tecketReservation = TRUE;
		strcpy(f[i].name, "");
		strcpy(f[i].surname, "");
	}

	FILE *file = fopen("FlightReport.txt", "rt+");
	char arr[48][100];
	int count = 0;
	char c = '0';

	if (getc(file) != EOF)
	{
		fseek(file, 0,SEEK_SET);
		while (!feof(file))
		{
			fscanf(file, "%d",&f[count].idOfSeat);
			fscanf(file, "%d",&f[count].tecketReservation);
			fscanf(file, "%s", f[count].name);
			fscanf(file, "%s", f[count].surname);	
			fgetc(file);
			count++;	
		}
	}
	
	char inputVal = '0';

	while(TRUE)
	{
		menu();
		printf("\nChoose would you like to do:");
		
		scanf("%[^\n]c", &inputVal);
		system("clear");
		if (inputVal != 'e')
		{
			switch (inputVal)
			{
			case 'a':ShowTheFreeSeats(f); break;
			case 'b':ShowListOfFreeSeats(f); break;
			case 'c':ShowListOfReservationSeats(f); break;
			case 'd':ReservSeat(f); break;
			case 'f':CancelReserve(f); break;

			default: printf("\nNot corrected value,try again.\n");
			}
		}
		else break;
		//system("cls"); // заменить для мака
	}

	fclose(file);
	free(f);
	
	return 0;
}
